﻿namespace TIMS2
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.techNamelblP3 = new System.Windows.Forms.Label();
            this.enteredNIO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comP3 = new System.Windows.Forms.Button();
            this.creationP3 = new System.Windows.Forms.Button();
            this.docP3 = new System.Windows.Forms.Button();
            this.prepP3 = new System.Windows.Forms.Button();
            this.testP3 = new System.Windows.Forms.Button();
            this.l1P3 = new System.Windows.Forms.Label();
            this.l2P3 = new System.Windows.Forms.Label();
            this.l3P3 = new System.Windows.Forms.Label();
            this.l4P3 = new System.Windows.Forms.Label();
            this.l5P3 = new System.Windows.Forms.Label();
            this.notesP3 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(331, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIMS";
            // 
            // techNamelblP3
            // 
            this.techNamelblP3.AutoSize = true;
            this.techNamelblP3.Location = new System.Drawing.Point(12, 18);
            this.techNamelblP3.Name = "techNamelblP3";
            this.techNamelblP3.Size = new System.Drawing.Size(118, 13);
            this.techNamelblP3.TabIndex = 2;
            this.techNamelblP3.Text = "Tech Name from Form1";
            // 
            // enteredNIO
            // 
            this.enteredNIO.Location = new System.Drawing.Point(311, 80);
            this.enteredNIO.Name = "enteredNIO";
            this.enteredNIO.Size = new System.Drawing.Size(100, 20);
            this.enteredNIO.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 39);
            this.label2.TabIndex = 4;
            this.label2.Text = "Chart to be entered\r\nwhen database is\r\nbuilt and completed.";
            // 
            // comP3
            // 
            this.comP3.Location = new System.Drawing.Point(131, 114);
            this.comP3.Name = "comP3";
            this.comP3.Size = new System.Drawing.Size(110, 23);
            this.comP3.TabIndex = 5;
            this.comP3.Text = "Communication";
            this.comP3.UseVisualStyleBackColor = true;
            this.comP3.Click += new System.EventHandler(this.comP3_Click);
            // 
            // creationP3
            // 
            this.creationP3.Location = new System.Drawing.Point(131, 143);
            this.creationP3.Name = "creationP3";
            this.creationP3.Size = new System.Drawing.Size(110, 23);
            this.creationP3.TabIndex = 6;
            this.creationP3.Text = "Creation";
            this.creationP3.UseVisualStyleBackColor = true;
            this.creationP3.Click += new System.EventHandler(this.creationP3_Click);
            // 
            // docP3
            // 
            this.docP3.Location = new System.Drawing.Point(131, 172);
            this.docP3.Name = "docP3";
            this.docP3.Size = new System.Drawing.Size(110, 23);
            this.docP3.TabIndex = 7;
            this.docP3.Text = "Documentation";
            this.docP3.UseVisualStyleBackColor = true;
            this.docP3.Click += new System.EventHandler(this.docP3_Click);
            // 
            // prepP3
            // 
            this.prepP3.Location = new System.Drawing.Point(131, 201);
            this.prepP3.Name = "prepP3";
            this.prepP3.Size = new System.Drawing.Size(110, 23);
            this.prepP3.TabIndex = 8;
            this.prepP3.Text = "Preparation";
            this.prepP3.UseVisualStyleBackColor = true;
            this.prepP3.Click += new System.EventHandler(this.prepP3_Click);
            // 
            // testP3
            // 
            this.testP3.Location = new System.Drawing.Point(131, 230);
            this.testP3.Name = "testP3";
            this.testP3.Size = new System.Drawing.Size(110, 23);
            this.testP3.TabIndex = 9;
            this.testP3.Text = "Testing";
            this.testP3.UseVisualStyleBackColor = true;
            this.testP3.Click += new System.EventHandler(this.testP3_Click);
            // 
            // l1P3
            // 
            this.l1P3.AutoSize = true;
            this.l1P3.Location = new System.Drawing.Point(270, 119);
            this.l1P3.Name = "l1P3";
            this.l1P3.Size = new System.Drawing.Size(0, 13);
            this.l1P3.TabIndex = 10;
            // 
            // l2P3
            // 
            this.l2P3.AutoSize = true;
            this.l2P3.Location = new System.Drawing.Point(270, 148);
            this.l2P3.Name = "l2P3";
            this.l2P3.Size = new System.Drawing.Size(0, 13);
            this.l2P3.TabIndex = 11;
            // 
            // l3P3
            // 
            this.l3P3.AutoSize = true;
            this.l3P3.Location = new System.Drawing.Point(270, 177);
            this.l3P3.Name = "l3P3";
            this.l3P3.Size = new System.Drawing.Size(0, 13);
            this.l3P3.TabIndex = 12;
            // 
            // l4P3
            // 
            this.l4P3.AutoSize = true;
            this.l4P3.Location = new System.Drawing.Point(270, 206);
            this.l4P3.Name = "l4P3";
            this.l4P3.Size = new System.Drawing.Size(0, 13);
            this.l4P3.TabIndex = 13;
            // 
            // l5P3
            // 
            this.l5P3.AutoSize = true;
            this.l5P3.Location = new System.Drawing.Point(270, 235);
            this.l5P3.Name = "l5P3";
            this.l5P3.Size = new System.Drawing.Size(0, 13);
            this.l5P3.TabIndex = 14;
            // 
            // notesP3
            // 
            this.notesP3.Location = new System.Drawing.Point(321, 232);
            this.notesP3.Name = "notesP3";
            this.notesP3.Size = new System.Drawing.Size(228, 20);
            this.notesP3.TabIndex = 15;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(515, 116);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(34, 20);
            this.textBox1.TabIndex = 16;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(515, 145);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(34, 20);
            this.textBox2.TabIndex = 17;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(515, 174);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(34, 20);
            this.textBox3.TabIndex = 18;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(515, 203);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(34, 20);
            this.textBox4.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(495, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 26);
            this.label3.TabIndex = 20;
            this.label3.Text = "Enter time\r\nin minutes";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(273, 50);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 21;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 294);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.notesP3);
            this.Controls.Add(this.l5P3);
            this.Controls.Add(this.l4P3);
            this.Controls.Add(this.l3P3);
            this.Controls.Add(this.l2P3);
            this.Controls.Add(this.l1P3);
            this.Controls.Add(this.testP3);
            this.Controls.Add(this.prepP3);
            this.Controls.Add(this.docP3);
            this.Controls.Add(this.creationP3);
            this.Controls.Add(this.comP3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.enteredNIO);
            this.Controls.Add(this.techNamelblP3);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label techNamelblP3;
        private System.Windows.Forms.TextBox enteredNIO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button comP3;
        private System.Windows.Forms.Button creationP3;
        private System.Windows.Forms.Button docP3;
        private System.Windows.Forms.Button prepP3;
        private System.Windows.Forms.Button testP3;
        private System.Windows.Forms.Label l1P3;
        private System.Windows.Forms.Label l2P3;
        private System.Windows.Forms.Label l3P3;
        private System.Windows.Forms.Label l4P3;
        private System.Windows.Forms.Label l5P3;
        private System.Windows.Forms.TextBox notesP3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}