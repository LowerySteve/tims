﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TIMS2
{
    public partial class Form3 : Form
    {
        public Data Data
        {
            set
            {
                enteredNIO.Text = value.NIONumber;
            }
        }
        
        public Form3(string comboBox1)
        {
            InitializeComponent();
            techNamelblP3.Text = comboBox1;
        }

        private void comP3_Click(object sender, EventArgs e)
        {
            l1P3.Text = "With Sales/QC/Purchasing Team";
            l2P3.Text = "Phone Conference Call";
            l3P3.Text = "Customer Visit On Site";
            l4P3.Text = "Customer Visit Off Site";
            l5P3.Text = "Notes:";

        }

        private void creationP3_Click(object sender, EventArgs e)
        {
            l1P3.Text = "Formulation";
            l2P3.Text = "Nutritional";
            l3P3.Text = "";
            l4P3.Text = "";
            l5P3.Text = "Notes:";
        }

        private void docP3_Click(object sender, EventArgs e)
        {
            l1P3.Text = "Saving Project Data";
            l2P3.Text = "Ingredients";
            l3P3.Text = "Nutritional Review";
            l4P3.Text = "Label Review";
            l5P3.Text = "Notes:";
        }

        private void prepP3_Click(object sender, EventArgs e)
        {
            l1P3.Text = "Dry Blends for Testing";
            l2P3.Text = "Dry Blends for Samples";
            l3P3.Text = "";
            l4P3.Text = "";
            l5P3.Text = "Notes:";
        }

        private void testP3_Click(object sender, EventArgs e)
        {
            l1P3.Text = "Bench Sensory";
            l2P3.Text = "Bench Functionality";
            l3P3.Text = "Pilot Plant";
            l4P3.Text = "Sensory";
            l5P3.Text = "Notes:";
        }
    }
}
