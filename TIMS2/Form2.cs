﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TIMS2
{
    public partial class Form2 : Form
    {
        public Data Data
        {
            set
            {
                techNamelblP2.Text = value.TechName;
            }
        }
        public Form2(string comboBox1)
        {
            InitializeComponent();
            techNamelblP2.Text = comboBox1;

        }
    }
}
